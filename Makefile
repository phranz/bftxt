# bftxt : a brainfuck string generator.
# Written by Francesco Palumbo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

all:
	cc -o bftxt -D_POSIX_C_SOURCE_2 -Wall -O2 bftxt.c

install:
	install -m 0755 bftxt /usr/bin
	install -m 0755 COPYING /usr/share/doc/bftxt

clean:
	unlink bftxt
